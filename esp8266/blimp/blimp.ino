#include <Arduino.h>

#include <Hash.h>
#include <FS.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <WebSocketsServer.h>

#include <Servo.h>

#include "debug.h"
#include "server.h"

// PINS

const int HEIGHT_SA1 = D0;
const int HEIGHT_SA2 = D1;
const int HEIGHT_PWM = D2;

const int THRUST_SA1 = D3;
const int THRUST_SA2 = D4;
const int THRUST_PWM = D5;

const int SERVO_CAM = D6;
const int SERVO_DIR = D7;

Servo servo_cam;
Servo servo_dir;

// WiFi AP parameters
char ap_ssid[13];
char* ap_password = "";

// WiFi STA parameters
char* sta_ssid = "...";
char* sta_password = "...";

//
// Setup //
//

void setupPins() {
    // setup Serial and actuators
    Serial.begin(115200);
    DEBUG("Started serial.");

    pinMode(THRUST_SA1,OUTPUT);  //AIN1
    pinMode(THRUST_SA2,OUTPUT);  //AIN2
    pinMode(THRUST_PWM,OUTPUT);  //PWM

    pinMode(HEIGHT_SA1,OUTPUT);  //AIN2
    pinMode(HEIGHT_SA2,OUTPUT);  //AIN1
    pinMode(HEIGHT_PWM,OUTPUT);  //PWM

    pinMode(SERVO_CAM,OUTPUT);  //Servo
    pinMode(SERVO_DIR,OUTPUT);  //Servo

    servo_cam.attach(SERVO_CAM);
    servo_dir.attach(SERVO_DIR);
    servo_cam.write(90);
    servo_cam.write(90);
    DEBUG("Setup pins");
}

//
// Movement Functions //
//

void setmotors(const uint8_t payload[9]) {
  // Thrust
  digitalWrite(THRUST_SA1, payload[1]);
  digitalWrite(THRUST_SA2, payload[2]);
  analogWrite (THRUST_PWM, payload[3]);

  // Steering
  servo_dir.write(payload[4]);

  // Vertical
  digitalWrite(HEIGHT_SA1, payload[5]);
  digitalWrite(HEIGHT_SA2, payload[6]);
  analogWrite (HEIGHT_PWM, payload[7]);

  // Camera
  servo_cam.write(payload[8]);
}

void stop() {
    const static uint8_t off[9] = {0,0,0,0,0,0,0,0,0};
    setmotors(off);
}

void webSocketEvent(uint8_t id, WStype_t type, uint8_t * payload, size_t length) {
    char ipstr[16];

    switch(type) {
        case WStype_DISCONNECTED:
            DEBUG("Web socket disconnected, id = ", id);
            break;

        case WStype_CONNECTED: 
            // IPAddress ip = webSocket.remoteIP(id);
            // sprintf(ipstr, "%d.%d.%d.%d", ip[0], ip[1], ip[2], ip[3]);
            DEBUG("Web socket connected, id = ", id);
            // DEBUG("  from: ", ipstr);
            DEBUG("  url: ", (char *)payload);

            // send message to client
            wsSend(id, "Connected to ");
            wsSend(id, ap_ssid);
            break;

        case WStype_BIN:
            DEBUG("On connection #", id)
            DEBUG("  got binary of length ", length);
            for (int i = 0; i < length; i++)
              DEBUG("    char : ", payload[i]);
            if (payload[0] == '~' && length == 9) 
              setmotors(payload);
            break;

        case WStype_TEXT:
            DEBUG("On connection #", id)
            DEBUG("  got text: ", (char *)payload);
            break;
    }
}

void setup() {
    setupPins();

    sprintf(ap_ssid, "ESP_%08X", ESP.getChipId());

    for(uint8_t t = 4; t > 0; t--) {
        Serial.printf("[SETUP] BOOT WAIT %d...\n", t);
        Serial.flush();
    }
    //setupSTA(sta_ssid, sta_password);
    setupAP(ap_ssid, ap_password);
    setupWS(webSocketEvent);
    stop();
}

void loop() {
    wsLoop();
}
